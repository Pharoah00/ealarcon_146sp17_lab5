import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class TestFrame extends JFrame {

	private JPanel contentPane;
	private JTextField textHelloWorld;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TestFrame frame = new TestFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TestFrame() {
		setTitle("myTestFrame");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JButton btnNorth = new JButton("North");
		
		//define and register an event handler by using
		//an anonymous inner class
		btnNorth.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				textHelloWorld.setText( "You Clicked North!" );
			}
		});
		contentPane.add(btnNorth, BorderLayout.NORTH);
		
		JButton btnSouth = new JButton("South");
		btnSouth.addActionListener( new ClickListener("You Clicked South!" ) );
		contentPane.add(btnSouth, BorderLayout.SOUTH);
		
		JButton btnWest = new JButton("West");
		contentPane.add(btnWest, BorderLayout.WEST);
		
		JButton btnEast = new JButton("East");
		btnEast.addActionListener( new ActionListener());
		contentPane.add(btnEast, BorderLayout.EAST);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(null); //if null, uses AbsoluteLayout
		
		textHelloWorld = new JTextField();
		textHelloWorld.setText("Hello World");
		textHelloWorld.setBounds(10, 11, 294, 20);
		panel.add(textHelloWorld);
		textHelloWorld.setColumns(10);
	} //end constructor for TestFrame
	
	class ClickListener implements ActionListener
	{
		String message;
		@Override
		public ClickListener( String Message )
		{
			this.message = message;
		}
		@Override
		public void actionPerformed(ActionEvent arg0) 
		{
			textHelloWorld.setText( "You Clicked North!" );
		} 		

	} //end inner class ClickListener
	
	class AnotherClickListener implements ActionListener;
	{
		@Override
		public void actionPerformed( Action e )
		{
			JOptionPane.showMessageDialog( contentPane, "You clicked a button!" );
		}
	}
} //end class TestFrame
